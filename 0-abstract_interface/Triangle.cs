﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sisharp
{
    class Triangle : AShape, IArea
    {
        protected string name;
        protected string type;
        protected int a;
        protected int b;
        protected int c;
        

        public Triangle(string type, string name, int a, int b, int c)
        {
            this.type = type;
            this.name = name;
            this.a = a;
            this.b = b;
            this.c = c;
        }

        public double computeArea()
        {
            double halfPerimeter = (a + b + c) / 2;
            int area = (int)Math.Sqrt(halfPerimeter * (halfPerimeter - a) * (halfPerimeter - b) * (halfPerimeter - c));
            return area;
        }
        public override void print()
        {
            Console.WriteLine("Type: " + type);
            Console.WriteLine("Name: " + name);
            Console.WriteLine("A: " + a);
            Console.WriteLine("B: " + b);
            Console.WriteLine("C: " + c);
            Console.WriteLine("Area: " + computeArea());
        }
    }
}
