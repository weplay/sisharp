﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sisharp
{
    class Cube : AShape, IArea, IVolume
    {
        protected string name;
        protected string type;
        protected int a;
        protected double volume;

        public Cube(string type, string name, int a)
        {
            this.type = type;
            this.name = name;
            this.a = a;
        }

        public double computeArea()
        {
            return 6 * a * a;
        }
        public double computeVolume()
        {
            volume = a * a * a;
            return volume;
        }
        public override void print()
        {
            Console.WriteLine("Type: " + type);
            Console.WriteLine("Name: " + name);
            Console.WriteLine("A: " + a);
            Console.WriteLine("Area: " + computeArea());
            Console.WriteLine("Volume: " + computeVolume());
        }
    }
}
