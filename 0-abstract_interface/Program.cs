﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Collections;

namespace sisharp
{
    class Program
    {
        static void Main(string[] args)
        {
            AShape triangle = new Triangle("2d shape", "triangle", 3, 4, 5);
            AShape rectangle = new Rectangle("2d shape", "rectangle", 3, 4);
            AShape cube = new Cube("3d shape", "cube", 3);
            IVolume shapeVolume;

            List<AShape> list = new List<AShape>();
            list.Add(triangle);
            list.Add(rectangle);
            list.Add(cube);
            for(int i = 0; i < list.Count; i++)
            {
                shapeVolume = list[i] as IVolume;
                if (shapeVolume != null)
                {
                    Console.WriteLine(shapeVolume.computeVolume());
                }
            }            

        }
    }
}
