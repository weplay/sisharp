﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace sisharp
{
    class Rectangle : AShape, IArea
    {
        protected string name;
        protected string type;
        protected int a;
        protected int b;


        public Rectangle(string type, string name, int a, int b)
        {
            this.type = type;
            this.name = name;
            this.a = a;
            this.b = b;
        }

        public double computeArea()
        {
            return a * b;
        }
        public override void print()
        {
            Console.WriteLine("Type: " + type);
            Console.WriteLine("Name: " + name);
            Console.WriteLine("A: " + a);
            Console.WriteLine("B: " + b);
            Console.WriteLine("Area: " + computeArea());
        }
    }
}
