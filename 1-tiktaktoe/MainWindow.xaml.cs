﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;

namespace WPFtutorial
{
    public partial class MainWindow : Window
    {
        protected List<Button> gameButtons;
        protected int checker;

        public MainWindow()
        {
            InitializeComponent();
            gameButtons = new List<Button>();
            gameButtons.Add(A1);
            gameButtons.Add(A2);
            gameButtons.Add(A3);
            gameButtons.Add(B1);
            gameButtons.Add(B2);
            gameButtons.Add(B3);
            gameButtons.Add(C1);
            gameButtons.Add(C2);
            gameButtons.Add(C3);
            checker = 1;
            foreach(Button button in gameButtons)
            {
                button.Content = " ";
                button.IsEnabled = true;
            }
        }

        private void btnClick(object sender, RoutedEventArgs e)
        {
            Button currentButton = (Button)sender;
            if((checker % 2) == 0)
            {
                currentButton.Content = "O";
                currentButton.IsEnabled = false;
            }
            else
            {
                currentButton.Content = "X";
                currentButton.IsEnabled = false;
            }
            if(checkWiner() != "")
            {
                MessageBox.Show(checkWiner());
            }
            checker++;
        }

        private string checkWiner()
        {
            string winner = "";
            string win = " Win";
            if((!A1.Content.Equals(" ") && A1.Content.Equals(A2.Content) && A1.Content.Equals(A3.Content)) ||    //check A1A2A3
                (!A1.Content.Equals(" ") && A1.Content.Equals(B1.Content) && A1.Content.Equals(C1.Content)) ||//check A1B1C1
                (!A1.Content.Equals(" ") && A1.Content.Equals(B2.Content) && A1.Content.Equals(C3.Content)))  //check A1B2C3
            {
                winner = Convert.ToString(A1.Content) + win;
            }
            else if(!B1.Content.Equals(" ") && B1.Content.Equals(B2.Content) && B1.Content.Equals(B3.Content)) //check B1B2B3
            {
                winner = Convert.ToString(B1.Content) + win;
            }
            else if(!C1.Content.Equals(" ") && C1.Content.Equals(C2.Content) && C1.Content.Equals(C3.Content)) //check C1C2C3
            {
                winner = Convert.ToString(C1.Content) + win;
            }
            else if(!A2.Content.Equals(" ") && A2.Content.Equals(B2.Content) && A2.Content.Equals(C2.Content)) //check A2B2C2
            {
                winner = Convert.ToString(A2.Content) + win;
            }
            else if((!A3.Content.Equals(" ") && A3.Content.Equals(B3.Content) && A3.Content.Equals(C3.Content)) || //check A3B3C3
                (!A3.Content.Equals(" ") && A3.Content.Equals(B2.Content) && A3.Content.Equals(C1.Content))) //check A3B2C1
            {
                winner = Convert.ToString(A3.Content) + win;
            }
            return winner;
        }

    }
}
